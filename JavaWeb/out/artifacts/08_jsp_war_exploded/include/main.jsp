<%--
  Created by IntelliJ IDEA.
  User: feng'zhi'ren
  Date: 2022/9/14
  Time: 12:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
头部信息<br>
主体内容<br>
<%--
<%@ include file=""%> 就是【静态包含】(常用)
file 属性指定你要包含的jsp路径
   地址中第一个斜杠 / 表示http://ip:port/工程路径/  映射到代码就是web目录

   静态包含的特点:
   1. 静态包含不会翻译被包含的jsp页面
   2. 静态包含其实是把被包含的jsp页面的代码 【拷贝】到[包含的位置]执行输出
--%>
<%--<%@ include file="/include/footer.jsp"%>--%>


<%--
<jsp:include page=""></jsp:include> 这是【动态包含】
     page属性 是指定你要包含的jsp页面的路径
     动态包含也可以像静态包含一样，把被包含的内容执行输出到包含位置

     动态包含的特点:
     1. 动态包含会把包含的 jsp 页面翻译成java 代码
     2. 动态包含底层代码使用如下代码去调用 被包含的 jsp 页面执行输出
        JspRuntimeLibrary.include(request, response, "/include/footer.jsp", out, false);
     3. 动态包含，还可以[传递参数]
--%>
<jsp:include page="/include/footer.jsp">
    <jsp:param name="username" value="fzr"/>
    <jsp:param name="password" value="123"/>
</jsp:include>

</body>
</html>
