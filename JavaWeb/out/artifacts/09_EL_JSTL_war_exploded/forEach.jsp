<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.atfzr.pojo.Student" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: feng'zhi'ren
  Date: 2022/9/15
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%--1.遍历 1 到 10，输出
begin 属性设置开始的索引
end 属性设置结束的索引
var 属性表示循环的变量(也是当前正在遍历到的数据)
for (int i = 1; i < 10; i++) --%>
<table>
    <c:forEach begin="1" end="10" var="i">
        <tr>
            <td>
                第${ i }行
            </td>
        </tr>
    </c:forEach>
</table>
<hr>

<%-- 2.遍历Object 数组
     for(Object item : arr)
     items 表示遍历的数据源(遍历的集合)
     var 表示当前遍历到的数据--%>
<%
    request.setAttribute("arr", new String[]{"111", "666", "999"});
%>
<c:forEach items="${requestScope.arr}" var="item">
    ${item} <br>
</c:forEach>
<hr>

<% /*3.遍历map集合*/
    Map<String, Object> map = new HashMap<>();
    map.put("key1", "value1");
    map.put("key2", "value2");
    map.put("key3", "value3");
//    for(Map.Entry<String,Object> entry : map.entrySet()){
//    }
    request.setAttribute("map", map);
%>
<c:forEach items="${requestScope.map}" var="entry">
    ${entry}<br>
</c:forEach>
<hr>

<%-- 4. 遍历List集合 --list中存放Student类， 有属性: 编号，用户名，密码，年龄，电话信息--%>
<%
    ArrayList<Student> list = new ArrayList<>();
    for (int i = 1; i < 10; i++) {
        list.add(new Student(i, "username" + i, "pass" + i, 18 + i, "phone" + i));
    }
    request.setAttribute("stu", list);
%>
<table>
    <tr>
        <th>编号</th>
        <th>用户名</th>
        <th>密码</th>
        <th>年龄</th>
        <th>电话</th>
        <th>操作</th>
    </tr>

    <%--items 表示遍历的集合
    var 表示遍历到的数据
    begin 表示遍历的开始索引值
    end 表示结束的索引值
    step 属性表示遍历的步长值
    varStatus 属性表示当前遍历到的数据的状态
    for（int i = 1; i < 10; i+=2） --%>
    <c:forEach begin="2" end="8" step="2" varStatus="status" items="${requestScope.stu}" var="student">
           <tr>
               <td>${student.id}</td>
               <td>${student.username}</td>
               <td>${student.password}</td>
               <td>${student.age}</td>
               <td>${student.phone}</td>
               <td>${status.step}</td>
        </tr>
        </c:forEach>
</table>


</body>
</html>
