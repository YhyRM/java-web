<%--
  Created by IntelliJ IDEA.
  User: feng'zhi'ren
  Date: 2022/9/14
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Map<String,Object> map = new HashMap<String, Object>();
    map.put("a", "aaaValue");
    map.put("b+b+b", "bbbValue");
    map.put("c-c-c", "cccValue");

    request.setAttribute("map", map);
%>

${ map.a } <br>
<%-- 像这些key带有有歧义的字符时，我们不能使用.key运行。 要改用[]和""包住key--%>
${ map["b+b+b"] } <br>
${ map['c-c-c'] } <br>

</body>
</html>
