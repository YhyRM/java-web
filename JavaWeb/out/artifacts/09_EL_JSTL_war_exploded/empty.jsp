<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: feng'zhi'ren
  Date: 2022/9/14
  Time: 16:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    //    empty 运算可以判断一个数据是否为空，如果为空，则输出 true,不为空输出 false。
//    以下几种情况为空：
//    1、值为 null 值的时候，为空
    request.setAttribute("emptyValue", null);
//    2、值为空串的时候，为空
    request.setAttribute("emptyString", "");
//    3、值是 Object 类型数组，长度为零的时候
    request.setAttribute("emptyArray", new Object() {});
//    4、list 集合，元素个数为零
    ArrayList<Object> list = new ArrayList<>();
    request.setAttribute("emptyList", list);
//    5、map 集合，元素个数为零
    HashMap<String, Object> map = new HashMap<>();
    request.setAttribute("emptyMap", map);
%>
<%--  empty 判断域值是否为空--%>
${empty emptyValue}<br>
${empty emptyString}<br>
${empty emptyArray}<br>
${empty emptyList}<br>
${empty emptyMap}<br>
</body>
</html>
