package com.atfzr;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/10---9:31
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class HelloServlet implements Servlet {

    //第1,2步，是在第一次访问的时候 创建servlet程序会调用
    public HelloServlet() {
        System.out.println("1 构造器方法");
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        //重写init方法里面一定要调用父类的init（ServletConfig）操作
//        super.init(config);
        System.out.println("2 init初始化方法");

        //1. 可以获取Servlet程序的【别名】servlet.name的值
        System.out.println("HelloServlet程序的别名是: " + servletConfig.getServletName()); //HelloServlet
        //2. 获取初始化参数init-param
        System.out.println("初始化参数username是: " + servletConfig.getInitParameter("username")); // root
        System.out.println("初始化参数url是: " + servletConfig.getInitParameter("url")); //jdbc:mysql://localhost:3306/test
        //3. 获取ServletContext对象
        System.out.println(servletConfig.getServletContext()); //org.apache.catalina.core.ApplicationContextFacade@7de436d1

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    /**
     * service方法是专门处理请求和响应的
     *
     * @param servletRequest
     * @param servletResponse
     * @throws ServletException
     * @throws IOException
     */
    //第三步， 每次访问都会调用
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("3 service === HelloServlet被访问了");

        //因为要调用 【ServletRequest 子类】的获取请求方式的方法，所以要向下转型
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        // 获取请求的方法
        String method = httpServletRequest.getMethod();
        if ("GET".equals(method)) {
            doGet();
        } else if ("POST".equals(method)) {
            doPost();
        }

    }

    /***
     * 做 get 请求的操作
     * */
    public void doGet () {
        System.out.println("get 请求");
        System.out.println("get 请求");
    }
    /***
     * 做 get 请求的操作
     * */
    public void doPost () {
        System.out.println("post 请求");
        System.out.println("post 请求");
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    //在web工程停止的时候调用
    public void destroy() {
        System.out.println("4 destroy销毁方法");
    }
}
