package com.atfzr;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/10---15:18
 * @SuppressWarnings({"all"})
 */
public class ServletContext01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletContext01的get方法");

        ServletContext servletContext = getServletContext();
        System.out.println("保存之前: Context1 获取 key 的值是: " + servletContext.getAttribute("key"));
        servletContext.setAttribute("key","value");

        System.out.println("Context1 中获取域数据 key 的值是: " + servletContext.getAttribute("key"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
