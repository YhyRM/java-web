package com.atfzr;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/10---14:37
 * @SuppressWarnings({"all"}) )ServletContext 类的四个作用
 * 1、获取 web.xml 中配置的上下文参数 context-param
 * 2、获取当前的工程路径，格式: /工程路径
 * 3、获取工程部署后在服务器硬盘上的绝对路径
 * 4、像 Map 一样存取数据
 */
@SuppressWarnings({"all"})
public class ServletContext extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletContext的get方法");
//                  1、获取 web.xml 中配置的上下文参数 context-param
        javax.servlet.ServletContext servletContext = getServletConfig().getServletContext();
        String username = servletContext.getInitParameter("username");
        System.out.println("context-param 参数 username 的值是: " + username);
        System.out.println("context-param 参数 password 的值是: " + servletContext.getInitParameter("password"));
//                * 2、获取当前的工程路径，格式: /工程路径
        System.out.println("当前工程路径" + servletContext.getContextPath()); ///06_servlet
//                * 3、获取【工程】部署后在服务器硬盘上的绝对路径
        /**
         *  ( / 斜杠)被服务器解析地址为: http://ip:port/工程名/  【映射到IDEA代码的 web目录】
         */
        System.out.println("绝对路径" + servletContext.getRealPath("/")); //D:\Projects\JavaWeb\out\artifacts\06_servlet_war_exploded\
        System.out.println("工程下 css 目录的绝对路径是:" + servletContext.getRealPath("/css"));
        System.out.println("工程下 imgs 目录 1.jpg 的绝对路径是:" + servletContext.getRealPath("/imgs/hs.jpg"));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletContext的put方法");

    }
}
