package com.atfzr.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/10---11:01
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class HelloServlet2 extends HttpServlet {

    /**
     *  doGet()在get请求时调用
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    //获取表单提交方式
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("HelloServlet2 的doGet方法");

        ServletConfig servletConfig = getServletConfig();
        System.out.println(servletConfig); //ServletConfig对象: org.apache.catalina.core.StandardWrapperFacade@60c0ac58
    }

    /**
     *  doPost()在post请求时调用
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("HelloServlet2 的doPost方法");


    }
}
