package com.atfzr.utils;

import javax.servlet.http.Cookie;

/**
 * @author silence
 * @date 2022/9/19---18:40
 * @SuppressWarnings({"all"})
 */
public class CookieUtils {

    /**
     *  查到指定名称的cookie对象
     * @param name
     * @param cookies
     * @return
     */
    public static Cookie findCookie(String name, Cookie[] cookies){
        if(name == null || cookies == null || cookies.length == 0){
            return  null;
        }

        for(Cookie cookie : cookies){
            if(name.equals(cookie.getName())){
                //找到就返回，不然就一直找
                return cookie;
            }
        }
        return  null;
    }
}
