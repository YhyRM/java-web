package com.atfzr.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/20---13:02
 * @SuppressWarnings({"all"})   用cookie实现免用户名登录
 */
@SuppressWarnings({"all"})
public class loginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        if("fzr666".equals(username) && "123456".equals(password)){
            //登录成功 (回显) , 获取到用户名并加入到cookie通知客户端保存
            Cookie cookie = new Cookie("username", username);
            cookie.setMaxAge(60*60*24*7); //显示7天，（防止关浏览器后再登录就不显示了）
            resp.addCookie(cookie);
            System.out.println("登陆成功");

        }else {
            //登录失败
            System.out.println("登录失败");
        }
    }
}
