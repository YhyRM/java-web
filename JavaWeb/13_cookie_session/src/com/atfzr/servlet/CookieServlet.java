package com.atfzr.servlet;

import com.atfzr.utils.CookieUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/19---11:37
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class CookieServlet extends  BaseServlet{

    protected void testPath(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("path1", "path1");
        // getContextPath() ===> 得到工程路径
        cookie.setPath(req.getContextPath() + "/abc"); // ===> /工程路径/abc
        resp.addCookie(cookie);
        resp.getWriter().write("创建了一个带有Path路径的Cookie");

    }

    //修改cookie值
    protected void updateCookie(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //方案一: 1. 添加【相同cookie】到构造器就相当于修改
        Cookie cookie = new Cookie("key1", "newValue1");
        resp.addCookie(cookie);
        //2. 调用 response.addCookie()通知客户端保存修改
        resp.getWriter().write("修改Cookie值的方法被调用~");

        //方案二: 1.找到需要修改的Cookie对象，用setValue()方法修改。
        Cookie[] cookies = req.getCookies();
        Cookie key3 = CookieUtils.findCookie("key3", cookies);

        if(key3 != null){
            //setValue()
            key3.setValue("newValue3");
            //2. 调用 response.addCookie()通知客户端保存修改
            resp.addCookie(key3);
        }

    }

    // 设置Cookie存活 一小时
    protected void life3600(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("life3600", "life3600");
        cookie.setMaxAge(60 * 60);
        resp.addCookie(cookie);
        resp.getWriter().write("cookie已经存活了一小时");
    }

    // 设置cookie立刻删除
    protected void nowDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie nowdelete = new Cookie("life", "life");
        nowdelete.setMaxAge(0);
        resp.addCookie(nowdelete);
    }

    // cookie的默认存活时间
    protected void defaultLife(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie defaultLife = new Cookie("defaultLife", "defaultLife");
        defaultLife.setMaxAge(-5);
        resp.addCookie(defaultLife);
    }


    //获取客户端(浏览器)发送过来的cookie对象
    protected void getCookie(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();

        for (Cookie cookie : cookies){
            // getName方法返回Cookie的key(名)
            // getValue方法返回Cookie的value值
           resp.getWriter().write("Cookie[" +cookie.getName()+ "=" + cookie.getValue() +"]<br/>");
        }


        Cookie iWantCookie = CookieUtils.findCookie("key4",cookies);
        //以下寻找判断的操作可以封装成工具类
//        for(Cookie cookie:cookies) {
//            if ("key1".equals(cookie.getName())) {
//                iWantCookie = cookie;
//                //一找到就跳出循环
//                break;
//            }
//        }

        //如果为null，说明赋过值，也就是说明找到了所需的cookie
        if(iWantCookie != null){
            resp.getWriter().write("找到了需要的cookie");
        }else {
            resp.getWriter().write("没找到需要的cookie");
        }

    }

    // 创建cookie对象
    protected void createCookie(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("key1", "value1");
        Cookie cookie1 = new Cookie("key3", "value3");
        //通知客户端保存cookie
        resp.addCookie(cookie);
        resp.addCookie(cookie1);
        resp.getWriter().write("Cookie创建成功");
    }
}
