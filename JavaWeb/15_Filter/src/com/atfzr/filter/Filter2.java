package com.atfzr.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/24---11:07
 * @SuppressWarnings({"all"})
 */
public class Filter2 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter2前置代码");
        System.out.println("Filter2的线程= " + Thread.currentThread().getName());
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("Filter2的线程= " + Thread.currentThread().getName());
        // 若doFilter()执行，则后置代码执行。 否则后置代码不执行，直接跳到1的后置代码再结束
        System.out.println("Filter2后置代码");
    }

    @Override
    public void destroy() {

    }
}
