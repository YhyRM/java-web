package com.atfzr.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/23---22:23
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class AdminFilter implements Filter {

    public AdminFilter() {
        System.out.println("Filter的构造器方法");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter的初始化方法");
//        1、获取 Filter 的名称 filter-name 的内容
        System.out.println("filter-name=" + filterConfig.getFilterName());
//        2、获取在 Filter 中配置的 init-param 初始化参数(用的多)
        System.out.println("init-parm参数 username= " + filterConfig.getInitParameter("username"));
        System.out.println("init-parm参数 url= " + filterConfig.getInitParameter("url"));
//        3、获取 ServletContext 对象
        System.out.println(filterConfig.getServletContext());
    }

    /**
     * doFilter方法 可以用来拦截请求，过滤响应。  权限检查
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter的过滤方法");
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        HttpSession session = httpServletRequest.getSession();
        Object user = session.getAttribute("user");
        //如果user等于null， 说明还没有登录(没权限)
        if(user == null){
            //跳转回登录页面
            servletRequest.getRequestDispatcher("/login.jsp").forward(servletRequest,servletResponse);
            return;
        }else {
            // 让程序继续往下访问用户的目标资源
            filterChain.doFilter(servletRequest,servletResponse);
        }

    }

    @Override
    public void destroy() {
        System.out.println("Filter的销毁方法");
    }
}
