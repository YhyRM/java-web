package com.atfzr.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/24---11:07
 * @SuppressWarnings({"all"})
 */
public class Filter1 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filter1前置代码");
        //多个过滤器执行时线程一致 http-nio-8080-exec-30   都使用同一个Request对象
        System.out.println("Filter1的线程= " + Thread.currentThread().getName());
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("Filter1的线程= " + Thread.currentThread().getName());
        System.out.println("Filter1后置代码");

    }

    @Override
    public void destroy() {

    }
}
