import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author silence
 * @date 2022/9/11---10:16
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class ParameterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("=================doGet=================");
        //获取请求的参数信息
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String[] hobby = req.getParameterValues("hobby"); //因为hobby有多个所以用数组保存

        System.out.println("用户名=" + username);
        System.out.println("密码="  + password);
        //以数组的形式输出
        System.out.println("兴趣= " + Arrays.asList(hobby));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置请求体的字符集为UTR-8，从而解决post请求的中文乱码问题
        // 获取参数之前 调用才有效(所以一般写在代码块的最顶端)
        req.setCharacterEncoding("UTF-8");
        System.out.println("=================doPost=================");
        //获取请求的参数信息
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String[] hobby = req.getParameterValues("hobby");

        System.out.println("用户名=" + username);
        System.out.println("密码="  + password);
        //以数组的形式输出
        System.out.println("兴趣= " + Arrays.asList(hobby));
    }
}
