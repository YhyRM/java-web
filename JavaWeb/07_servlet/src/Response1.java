import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/12---9:22
 * @SuppressWarnings({"all"})   请求重定向， 旧的地址
 */
@SuppressWarnings({"all"})
public class Response1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset-UTF-8");
        System.out.println("Response1 到此一游");
//        // 设置响应状态码302，表示重定向 (已搬迁)
//        resp.setStatus(302);
//        //设置响应头，说明 新的地址在哪里
//        resp.setHeader("location","http://localhost:8080/07_servlet/response2"); //访问 1时直接跳到response2

        //相比于上面两步，也可以直接一步到位请求重定向(经典白学)
        resp.sendRedirect("http://localhost:8080/07_servlet/response2");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
