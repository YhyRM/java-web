import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author silence
 * @date 2022/9/12---8:44
 * @SuppressWarnings({"all"})  响应的输出流 和 字符集设置
 */
@SuppressWarnings({"all"})
public class ResponseIOServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        //设置【服务器】 字符集为UTF-8
//        resp.setCharacterEncoding("UTF-8"); // 编码默认ISO-8859-1
//
//        //通过[响应头]，设置【浏览器】也使用UTF-8字符集
//        resp.setHeader("Content-type","text/html; charset-UTF-8");

        //这种方法相当于上面两步 同时设置服务器和浏览器的字符集
        // 它会同时设置【服务器和客户端】都使用 UTF-8 字符集，还设置了响应头
        resp.setContentType("text/html; charset-UTF-8");

        //两个输出流只能调用一个，同时调用(使用两个响应流)会报错!!!       xxx has already been called for this response
//        resp.getOutputStream();
//        resp.getWriter();
        //往客户端 回传字符串数据
        PrintWriter writer = resp.getWriter();
        writer.write("智哥真帅!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
