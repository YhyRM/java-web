package com.atfzr.web;

import com.atfzr.pojo.Cart;
import com.atfzr.pojo.User;
import com.atfzr.service.OrderService;
import com.atfzr.service.impl.OrderServiceImpl;
import com.atfzr.utils.JdbcUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/23---8:49
 * @SuppressWarnings({"all"})
 */
public class OrderServlet extends BaseServlet {
    private OrderService orderService = new OrderServiceImpl();

    /**
     * 生成订单
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void createOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 从域中获取cart对象
        Cart cart = (Cart) req.getSession().getAttribute("cart");
        // 从域中获取user对象
        User loginUser = (User) req.getSession().getAttribute("user");

        //要判断用户有没有登录，不然会空指针异常
        if (loginUser == null) {
            //请求转发到登录页面
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req, resp);
            return;
        }
        System.out.println("OrderServlet程序在[" + Thread.currentThread().getName() + "]中");

//        要调用orderService.createOrder(cart,userId)生成订单号
        String  orderId =   orderService.createOrder(cart, loginUser.getId());

//        try {
//            //给 Service方法加上try-catch实现事务管理
//          orderId =   orderService.createOrder(cart, loginUser.getId());
//
//            JdbcUtils.commitAndClose(); // 提交事务
//        } catch (Exception e) { //获取orderService 抛出的异常
//            JdbcUtils.rollbackAndClose(); // 回滚事务
//            e.printStackTrace();
//        }

//        //将订单号保存到 request域中
//        req.setAttribute("orderId",orderId);
//        // 请求转发
//        req.getRequestDispatcher("/pages/cart/checkout.jsp").forward(req,resp);

        //在结算页面刷新会刷新订单号(改用重定向)
        req.getSession().setAttribute("orderId", orderId);
        //重定向，防止刷新问题
        resp.sendRedirect(req.getContextPath() + "/pages/cart/checkout.jsp");
    }
}
