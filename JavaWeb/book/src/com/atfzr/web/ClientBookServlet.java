package com.atfzr.web;

import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;
import com.atfzr.service.BookService;
import com.atfzr.service.impl.BookServiceImpl;
import com.atfzr.utils.WebUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/19---8:15
 * @SuppressWarnings({"all"})
 */
public class ClientBookServlet extends BaseServlet{

    private BookService bookService = new BookServiceImpl();

    // 处理分页功能
    protected void page(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("经过了前台的BookServlet程序");
        //1. 获取请求的参数 pageNo 和 pageSize
        int pageNo = WebUtils.parseInt(req.getParameter("pageNo"), 1);
        int pageSize = WebUtils.parseInt(req.getParameter("pageSize"), Page.PAGE_SIZE);
        //2. 调用BookService.page(pageNo,pageSize): Page对象
        Page<Book> page  = bookService.page(pageNo,pageSize);
        page.setUrl("client/bookServlet?action=page");
        //3. 保存Page对象到Request域中
        req.setAttribute("page",page);
        //4. 请求转发到pages/manager/book_manager.jsp页面
        req.getRequestDispatcher("/pages/client/index.jsp").forward(req,resp);

    }

    // 处理价格区间查询
    protected void pageByPrice(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("经过了前台的BookServlet程序");
        //1. 获取请求的参数 pageNo 和 pageSize
        int pageNo = WebUtils.parseInt(req.getParameter("pageNo"), 1);
        int pageSize = WebUtils.parseInt(req.getParameter("pageSize"), Page.PAGE_SIZE);
        int min = WebUtils.parseInt(req.getParameter("min"),0);
        int max = WebUtils.parseInt(req.getParameter("max"),Integer.MAX_VALUE);

        //2. 调用BookService.page(pageNo,pageSize): Page对象
        Page<Book> page  = bookService.pageByPrice(pageNo,pageSize,min,max);

        //解决跳转页面后， 价格区间框中回显消失问题
        StringBuffer sb = new StringBuffer("client/bookServlet?action=pageByPrice");
        //如果有最小价格的参数，追加到地址栏中
        if(req.getParameter("min") != null){
            sb.append("&min=").append(req.getParameter("min"));
        }
        //如果有最大价格的参数，追加到地址栏中
        if(req.getParameter("max") != null){
            sb.append("&max=").append(req.getParameter("max"));
        }
        page.setUrl(sb.toString());
        //3. 保存Page对象到Request域中
        req.setAttribute("page",page);
        //4. 请求转发到pages/manager/book_manager.jsp页面
        req.getRequestDispatcher("/pages/client/index.jsp").forward(req,resp);

    }
}
