package com.atfzr.web;

import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;
import com.atfzr.service.BookService;
import com.atfzr.service.impl.BookServiceImpl;
import com.atfzr.utils.WebUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author silence
 * @date 2022/9/17---10:15
 * @SuppressWarnings({"all"})  图书管理: 实现修改，删除，添加,查询等功能(完成与jsp的交互)
 */
public class BookServlet extends BaseServlet{

    private BookService bookService = new BookServiceImpl();

    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int pageNo = WebUtils.parseInt(req.getParameter("pageNo"), 0);
        pageNo +=1; //为了解决添加后没跳到最后一页的问题
        //1. 获取请求的参数--封装成Book对象
        Book book = WebUtils.copyParamToBean(req.getParameterMap(),new Book());
        //2. 调用BookService.addBook() 保存图书
        bookService.addBook(book);
        //3. 跳到图书列表页面,并再次调用list  (/manager/bookServlet?action=list)
        //当用户提交完请求，浏览器会记录下最后一次请求的全部信息。当用户按下功能键 F5，就会发起浏览器记录的最后一次 请求
        //用请求转发会重复提交
//        req.getRequestDispatcher("/manager/bookServlet?action=list").forward(req,resp);

        //这里我们用重定向(响应两次请求)
        resp.sendRedirect(req.getContextPath() + "/manager/bookServlet?action=page&pageNo=" + pageNo);
//        resp.sendRedirect(req.getContextPath() + "/manager/bookServlet?action=page&pageNo=" + req.getParameter("pageNo"));
    }

    // 处理分页功能
    protected void page(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 获取请求的参数 pageNo 和 pageSize
        int pageNo = WebUtils.parseInt(req.getParameter("pageNo"), 1);
        int pageSize = WebUtils.parseInt(req.getParameter("pageSize"), Page.PAGE_SIZE);
        //2. 调用BookService.page(pageNo,pageSize): Page对象
       Page<Book> page  = bookService.page(pageNo,pageSize);
        page.setUrl("manager/bookServlet?action=page");
        //3. 保存Page对象到Request域中
        req.setAttribute("page",page);
        //4. 请求转发到pages/manager/book_manager.jsp页面
        req.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(req,resp);

    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 获取请求的参数== 封装称为Book对象
        Book book = WebUtils.copyParamToBean(req.getParameterMap(), new Book());
        //2. 调用BookService.updateBook(book):修改图书
        bookService.updateBook(book);
        //3. 重定向会图书列表管理页面 地址: /工程名/manager/bookServlet?action=list
        resp.sendRedirect(req.getContextPath() + "/manager/bookServlet?action=page&pageNo=" + req.getParameter("pageNo"));
    }

    protected void getBook(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 获取请求的参数图书编号
        int id = WebUtils.parseInt(req.getParameter("id"), 0);
        //2. 调用bookService.queryBookById查询图书
        Book book = bookService.queryBookById(id);
        //3. 保存图书到Request域(scope)中
        req.setAttribute("book",book);
        //4. 请求转发到 /pages/manager/book_edit.jsp页面
        req.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(req,resp);
    }

    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 获取请求的参数id，图书编程
        int id = WebUtils.parseInt(req.getParameter("id"), 0);
        // 2. 调用bookService.deleteBookById(); 删除图书
        bookService.deleteBookById(id);
        // 3. 重定向会图书列表管理页面  /book/manager/bookServlet?action = list
        resp.sendRedirect(req.getContextPath() + "/manager/bookServlet?action=page&pageNo=" + req.getParameter("pageNo"));
    }

    protected void list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 通过BookService查询全部图书
        List<Book> books = bookService.queryBooks();
        //2. 把全部图书保存到Request 域中
        req.setAttribute("books",books);
        //3. 请求转发到 /pages/manager/book_manager.jsp
        req.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(req,resp);
    }
}
