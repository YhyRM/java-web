package com.atfzr.web;

import com.atfzr.pojo.User;
import com.atfzr.service.UserService;
import com.atfzr.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/12---21:50
 * @SuppressWarnings({"all"})
 * servlet 不管多复杂 永远都是那四步:1.获取参数 2.调用Service 3.将数据共享到域 4.路径跳转
 */
public class RegistServlet extends HttpServlet {
    UserService userService = new UserServiceImpl();
//注册的时候需要重写doPos方法，登录的时候需要写doGet方法
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doget被调用");
    }

    //页面跳转用post
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //1. 获取请求的参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String code = req.getParameter("code");
        //2. 检查 验证码是否正确  写死要求验证码为: abcde
        if("abcde".equalsIgnoreCase(code)){
            //正确
            //3. 检查用户名是否可用
            if(userService.existsUsername(username)){
                //已存在，不可用,跳回注册  界面
                System.out.println("用户名["+username+"]已存在!");

                //把错误信息，和用户名 邮箱的表单项信息，保存到域中
                req.setAttribute("msg","用户名已存在!");
                req.setAttribute("username", username);
                req.setAttribute("email",email);

                req.getRequestDispatcher("/pages/user/regist.jsp").forward(req,resp);
            }else {
                //可用
                //  调用Service保存到数据库 并跳到注册成功页面 regist_success.jsp
                userService.registUser(new User(null,username,password,email));
                // 跳到注册成功页面 regist_success.jsp
                req.getRequestDispatcher("/pages/user/regist_success.jsp").forward(req, resp);
            }
        }else {
            //把错误信息，和用户名 邮箱的表单项信息，保存到域中
            req.setAttribute("msg","验证码错误!");
            req.setAttribute("username", username);
            req.setAttribute("email",email);

            //验证码不正确 跳回注册页面
            System.out.println("验证码["+code+"]不正确");
            //  第一个斜杠表示从web开始
            req.getRequestDispatcher("/pages/user/regist.jsp").forward(req,resp);
        }

    }
}
