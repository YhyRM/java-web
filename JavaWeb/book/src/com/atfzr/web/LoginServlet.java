package com.atfzr.web;

import com.atfzr.dao.UserDao;
import com.atfzr.dao.impl.UserDaoImpl;
import com.atfzr.pojo.User;
import com.atfzr.service.UserService;
import com.atfzr.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/13---13:07
 * @SuppressWarnings({"all"}) servlet 不管多复杂 永远都是那四步:1.获取参数 2.调用Service 3.将数据共享到域 4.路径跳转
 */
public class LoginServlet extends HttpServlet {
  private UserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //调用参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        // 查询数据库中有无匹配的用户名及密码
        User loginUser = userService.login(new User(null, username, password, null));
        if(loginUser == null){
            // 把错误信息，和【回显】的表单项信息，保存到request域中
            req.setAttribute("msg","用户名或密码错误!");
            req.setAttribute("username",username);

            //登录失败,跳回登录页面  第一个斜杠代表web目录
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req,resp);
        }else {
            //登录成功,跳到登录成功页面
            req.getRequestDispatcher("pages/user/login_success.jsp").forward(req,resp);
        }
    }
}
