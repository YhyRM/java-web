package com.atfzr.web;

import com.atfzr.pojo.User;
import com.atfzr.service.UserService;
import com.atfzr.service.impl.UserServiceImpl;
import com.atfzr.utils.WebUtils;
import com.google.gson.Gson;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY;

/**
 * @author silence
 * @date 2022/9/16---8:59
 * @SuppressWarnings({"all"}) 用一个Servlet实现多个功能(登录 ， 注册)。 隐藏域的使用!!!
 */
public class UserServlet extends BaseServlet {

    private UserService userService = new UserServiceImpl();

    protected void ajaxExistsUsername(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取请求的参数username
        String username = req.getParameter("username");
        //调用userService.existsUsername();
        boolean existsUsername = userService.existsUsername(username);
        // 把返回的结果封装成为map对象
        HashMap<String , Object> resultMap = new HashMap<>();
        resultMap.put("existsUsername",existsUsername);

        Gson gson = new Gson();
        String json = gson.toJson(resultMap);

        resp.getWriter().write(json);
    }


        //注销
    protected void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //销毁当前session对象
        //重定向回到首页或 登录页面
        req.getSession().invalidate();
        resp.sendRedirect(req.getContextPath());
    }

    //封装，便于维护,   可以下断点继续深入理解
    protected void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //调用参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        // 查询数据库中有无匹配的用户名及密码
        User loginUser = userService.login(new User(null, username, password, null));
        if (loginUser == null) {
            // 把错误信息，和【回显】的表单项信息，保存到request域中
            req.setAttribute("msg", "用户名或密码错误!");
            req.setAttribute("username", username);

            //登录失败,跳回登录页面  第一个斜杠代表web目录
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req, resp);
        } else {
            //登录成功,跳到登录成功页面
            // 保存 用户信息loginUser 到session域中。 为什么用session因为要每个页面都显示用户名(管辖范围不同)
            req.getSession().setAttribute("user",loginUser);
            //跳转到登录成功页面
            req.getRequestDispatcher("pages/user/login_success.jsp").forward(req, resp);
        }
    }

    protected void regist(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 这边用谷歌的验证码jar包来实现验证码的前端开发!!!
        // 获取 Session 中的验证码
        String token = (String) req.getSession().getAttribute(KAPTCHA_SESSION_KEY);
        // 删除 Session 中的验证码
        req.getSession().removeAttribute(KAPTCHA_SESSION_KEY);

        //处理注册需求
        //1. 获取请求的参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String code = req.getParameter("code");  //客户端的验证码

//        User user = new User();
//        测试代码, 输出
//        Map<String, String[]> parameterMap = req.getParameterMap();
//        for (Map.Entry<String,String[]> entry: parameterMap.entrySet()){
//            System.out.println(entry.getKey() + "=" + Arrays.asList(entry.getKey()));
//        }

        /** 把所有请求的参数都注入到user对象中*/
        User user = WebUtils.copyParamToBean(req.getParameterMap(), new User());

        //2. 检查 验证码是否正确  写死要求验证码为: abcde
        if (token != null && token.equalsIgnoreCase(code)) {
            //正确
            //3. 检查用户名是否可用
            if (userService.existsUsername(username)) {
                //已存在，不可用,跳回注册  界面
                System.out.println("用户名[" + username + "]已存在!");

                //把错误信息，和用户名 邮箱的表单项信息，保存到域中
                req.setAttribute("msg", "用户名已存在!");
                req.setAttribute("username", username);
                req.setAttribute("email", email);

                req.getRequestDispatcher("/pages/user/regist.jsp").forward(req, resp);
            } else {
                //可用
                //  调用Service保存到数据库 并跳到注册成功页面 regist_success.jsp
                userService.registUser(new User(null, username, password, email));
                // 跳到注册成功页面 regist_success.jsp
                req.getRequestDispatcher("/pages/user/regist_success.jsp").forward(req, resp);
            }
        } else {
            //把错误信息，和用户名 邮箱的表单项信息，保存到域中
            req.setAttribute("msg", "验证码错误!");
            req.setAttribute("username", username);
            req.setAttribute("email", email);

            //验证码不正确 跳回注册页面
            System.out.println("验证码[" + code + "]不正确");
            //  第一个斜杠表示从web开始
            req.getRequestDispatcher("/pages/user/regist.jsp").forward(req, resp);
        }
    }


}
