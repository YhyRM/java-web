package com.atfzr.filter;

import com.atfzr.utils.JdbcUtils;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/9/25---10:49
 * @SuppressWarnings({"all"})
 */
public class TransactionFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            //这个方法间接调用了OrderServlet里的创建订单方法
            filterChain.doFilter(servletRequest,servletResponse);

            JdbcUtils.commitAndClose();//提交
        } catch (IOException e) {
            JdbcUtils.rollbackAndClose(); //回滚
            e.printStackTrace();
            throw new RuntimeException(e); //把异常再抛给tomcat服务器 !!!  展示友好的错误页面
        }

    }

    @Override
    public void destroy() {

    }
}
