package com.atfzr.dao.impl;

import com.atfzr.dao.OrderItemDao;
import com.atfzr.pojo.OrderItem;
/**
 * @author silence
 * @date 2022/9/22---22:09
 * @SuppressWarnings({"all"})
 */
public class OrderItemDaoImpl extends BaseDao implements OrderItemDao {
    @Override
    public int saveOrderItem(OrderItem orderItem) {
        System.out.println("OrderItemDaoImpl������["+ Thread.currentThread().getName() + "]��");

        String sql = "insert into t_order_item(`name`,`count`,`price`,`total_price`,`order_id`)values(?,?,?,?,?)";
        return update(sql,orderItem.getName(),orderItem.getCount(),orderItem.getPrice(),orderItem.getTotalPrice(),orderItem.getOrderId());
    }
}
