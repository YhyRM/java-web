package com.atfzr.dao.impl;

import com.atfzr.dao.UserDao;
import com.atfzr.pojo.User;

/**
 * @author silence
 * @date 2022/9/12---17:08
 * @SuppressWarnings({"all"})   完成对数据库中【用户的查询和增加】操作(管理员操作)
 */
public class UserDaoImpl extends BaseDao implements UserDao  {
    @Override
    public User queryUserByUsername(String username) {
        String sql = "select `id`,`username`,`password`,`email` from t_user where username = ?";
        return queryForOne(User.class,sql,username);
    }

    @Override
    public User queryUserByUsernameAndPassword(String username, String password) {
        String sql = "select `id`,`username`,`password`,`email` from t_user where username = ? and password = ?";
        return queryForOne(User.class,sql,username,password);
    }

    @Override
    public int saveUser(User user) {
        String sql = "INSERT INTO t_user(username, password,email) VALUES(?,?,?)";
        return  update(sql,user.getUsername(),user.getPassword(),user.getEmail());
    }
}
