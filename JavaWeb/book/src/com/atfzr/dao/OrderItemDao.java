package com.atfzr.dao;

import com.atfzr.pojo.OrderItem;

/**
 * @author silence
 * @date 2022/9/22---21:53
 * @SuppressWarnings({"all"})
 */
public interface OrderItemDao {
    public int saveOrderItem(OrderItem orderItem);
}
