package com.atfzr.dao;

import com.atfzr.pojo.User;

/**
 * @author silence
 * @date 2022/9/12---17:00
 * @SuppressWarnings({"all"})  查询用户信息接口(主要是为了一堆人写代码时[规范统一]，不加接口也行)
 */
public interface UserDao {


    /**
     *   根据用户名查询用户信息
     * @param username 用户名
     * @return 如果返回null，说明没有这个用户，反之有这个用户
     */
    public User queryUserByUsername(String username);

    /**
     *  根据用户名和密码查询用户
     * @param username
     * @param password
     * @return 如果返回null，说明没有这个用户或密码错误，反之亦然
     */
    public User queryUserByUsernameAndPassword(String username, String password);

    /**
     *  保存用户信息
     * @param user
     * @return  返回-1 表示操作失败， 其它返回影响的行数
     */
    public int saveUser(User user);
}
