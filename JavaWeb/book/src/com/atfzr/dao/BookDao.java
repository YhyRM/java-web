package com.atfzr.dao;

import com.atfzr.pojo.Book;

import java.util.List;

/**
 * @author silence
 * @date 2022/9/17---8:55
 * @SuppressWarnings({"all"})  声明图书操作所需要的方法
 */
public interface BookDao {

    public int addBook(Book book);

    public int deleteBookById(Integer id);

    public int updateBook(Book book);

    public Book queryBookById(Integer id);

    public List<Book> queryBooks();

    Integer queryForPageTotalCount();

    List<Book> queryForPageItems(int begin, int pageSize);

    Integer queryForPageTotalCountByPrice(int min, int max);

    List<Book> queryForPageItemsByPrice(int begin, int pageSize, int min, int max);
}
