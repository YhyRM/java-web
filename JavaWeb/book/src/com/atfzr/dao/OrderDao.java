package com.atfzr.dao;

import com.atfzr.pojo.Order;

/**
 * @author silence
 * @date 2022/9/22---21:53
 * @SuppressWarnings({"all"})
 */
public interface OrderDao {
    public int saveOrder(Order order);
}
