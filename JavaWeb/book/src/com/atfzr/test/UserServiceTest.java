package com.atfzr.test;

import com.atfzr.pojo.User;
import com.atfzr.service.UserService;
import com.atfzr.service.impl.UserServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/12---21:29
 * @SuppressWarnings({"all"})  用户操作
 */
public class UserServiceTest {
    UserService userService  = new UserServiceImpl();

    @Test
    public void registUser() {
        userService.registUser(new User(null,"fff123","123456","fff123@qq.com"));
        userService.registUser(new User(null,"sss123","123456","sss123@qq.com"));
    }

    @Test
    public void login() {
        userService.login(new User(null,"fff123","123456",null));
    }

    @Test
    public void existsUsername() {
        if(userService.existsUsername("sss123")){
            System.out.println("用户已存在");
        }else {
            System.out.println("用户名可用!");
        }
    }
}