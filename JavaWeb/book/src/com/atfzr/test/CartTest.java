package com.atfzr.test;

import com.atfzr.pojo.Cart;
import com.atfzr.pojo.CartItem;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/22---9:18
 * @SuppressWarnings({"all"})
 */
public class CartTest {

    @Test
    public void addItem() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(2,"班花",1,new BigDecimal(250),new BigDecimal(250)));

        System.out.println(cart);
    }

    @Test
    public void deleteItem() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(2,"班花",1,new BigDecimal(250),new BigDecimal(250)));

        cart.deleteItem(1);

        System.out.println(cart);
    }

    @Test
    public void clear() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(2,"班花",1,new BigDecimal(250),new BigDecimal(250)));

        cart.deleteItem(1);

        cart.clear();

        System.out.println(cart);
    }

    @Test
    public void updateCount() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(2,"班花",1,new BigDecimal(250),new BigDecimal(250)));

        cart.updateCount(2,3);

        System.out.println(cart);
    }
}