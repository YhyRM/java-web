package com.atfzr.test;

import com.atfzr.pojo.Cart;
import com.atfzr.pojo.CartItem;
import com.atfzr.service.OrderService;
import com.atfzr.service.impl.OrderServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/23---8:46
 * @SuppressWarnings({"all"})
 */
public class OrderServiceTest {

    @Test
    public void createOrder() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(1,"java从入门到入狱",1,new BigDecimal(150),new BigDecimal(150)));
        cart.addItem(new CartItem(2,"班花",1,new BigDecimal(250),new BigDecimal(250)));

        OrderService orderService = new OrderServiceImpl();
        String orderId = orderService.createOrder(cart, 1);
        System.out.println("订单号= " + orderId);
    }
}