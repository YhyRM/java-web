package com.atfzr.test;

import com.atfzr.dao.impl.UserDaoImpl;
import com.atfzr.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/12---17:21
 * @SuppressWarnings({"all"})    管理员操作
 */
@SuppressWarnings({"all"})
public class UserDaoTest {
    UserDaoImpl userDao = null; //全局
    @Test
    public void queryUserByUsername() {
         userDao = new UserDaoImpl();
        if(userDao.queryUserByUsername("admin") == null){ //说明没有这个用户
            System.out.println("用户名可用");
        }else {
            System.out.println("用户名已存在!");
        }
    }

    @Test
    public void queryUserByUsernameAndPassword() {
        userDao = new UserDaoImpl();
        if(userDao.queryUserByUsernameAndPassword("admin","root") == null){ //说明没有这个用户
            System.out.println("用户名或密码错误，登录失败");
        }else {
            System.out.println("登陆成功");
        }
    }

    @Test
    public void saveUser() {
        System.out.println(userDao.saveUser(new User(null,"fzr168", "123456", "fzr168@qq.com")));
    }
}