package com.atfzr.test;

import com.atfzr.dao.BookDao;
import com.atfzr.dao.impl.BookDaoImpl;
import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/17---9:15
 * @SuppressWarnings({"all"})
 */
public class BookDaoTest {
    private BookDao bookDao = new BookDaoImpl();

    @Test
    public void addBook() {
        bookDao.addBook(new Book(null, "智哥为什么这么帅！", "191125", new BigDecimal(9999), 1100000, 0, null));
    }

    @Test
    public void deleteBookById() {
        bookDao.deleteBookById(21);
    }

    @Test
    public void updateBook() {
        bookDao.updateBook(new Book(22, "大家都可以这么帅！", "智哥", new BigDecimal(9999), 1100000, 0, null));
    }

    @Test
    public void queryBookById() {
        System.out.println(bookDao.queryBookById(22));
    }

    @Test
    public void queryBooks() {
        for (Book queryBook : bookDao.queryBooks()) {
            System.out.println(queryBook);
        }
    }

    @Test
    public void queryForPageTotalCount() {
        System.out.println(bookDao.queryForPageTotalCount());
    }

    @Test
    public void queryForPageTotalCoun0tByPrice() {
        System.out.println(bookDao.queryForPageTotalCountByPrice(10,50));
    }

    @Test
    public void queryForPageItems() {
//        System.out.println(bookDao.queryForPageItems(0,5));
        for (Book book : bookDao.queryForPageItems(0, 5) ) {
            System.out.println(book);
        }
    }

    @Test
    public void queryForPageItemsByPrice() {
//        System.out.println(bookDao.queryForPageItems(0,5));
        for (Book book : bookDao.queryForPageItemsByPrice(0, Page.PAGE_SIZE,10,50) ) {
            System.out.println(book);
        }
    }
}