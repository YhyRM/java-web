package com.atfzr.test;

import com.atfzr.dao.OrderDao;
import com.atfzr.dao.impl.OrderDaoImpl;
import com.atfzr.pojo.Order;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/22---22:05
 * @SuppressWarnings({"all"})
 */
public class OrderDaoTest {

    @Test
    public void saveOrder() {
        OrderDao orderDao = new OrderDaoImpl();
        orderDao.saveOrder(new Order("1234567890",new Date(),new BigDecimal(100),0, 1));
    }
}