package com.atfzr.test;

import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;
import com.atfzr.service.BookService;
import com.atfzr.service.impl.BookServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @author silence
 * @date 2022/9/17---9:42
 * @SuppressWarnings({"all"})  其实Dao层的测试过了就行了，因为业务层是调用Dao层的数据的
 */
public class BookServiceTest {
    private BookService bookService = new BookServiceImpl();

    @Test
    public void addBook() {
        bookService.addBook(new Book(null, "java从入门到入狱！", "191125",new BigDecimal(9999), 1100000, 0, null));
    }

    @Test
    public void deleteBookById() {
        bookService.deleteBookById(22);
    }

    @Test
    public void updateBook() {
        bookService.updateBook(new Book(22,"社会我国哥，人狠话不多！", "1125", new BigDecimal(999999), 10, 111110, null));
    }

    @Test
    public void queryBookById() {
        System.out.println(bookService.queryBookById(11));
    }

    @Test
    public void queryBooks() {
    }

    @Test
    public void page() {
        System.out.println(bookService.page(1,5));
    }

    @Test
    public void pageByPrice() {
        System.out.println(bookService.pageByPrice(1,Page.PAGE_SIZE,10,50));
    }
}