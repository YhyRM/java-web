package com.atfzr.test;

import com.atfzr.dao.OrderItemDao;
import com.atfzr.dao.impl.OrderItemDaoImpl;
import com.atfzr.pojo.OrderItem;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author silence
 * @date 2022/9/22---22:07
 * @SuppressWarnings({"all"})
 */
public class OrderItemDaoTest {

    @Test
    public void saveOrderItem() {
        OrderItemDao orderItemDao = new OrderItemDaoImpl();
        orderItemDao.saveOrderItem(new OrderItem(null, "java 从入门到精通", 1, new BigDecimal(100), new BigDecimal(100), "1234567890"));
        orderItemDao.saveOrderItem(new OrderItem(null, "javaScript 从入门到精通", 2, new BigDecimal(100), new BigDecimal(200), "1234567890"));
        orderItemDao.saveOrderItem(new OrderItem(null, "Netty 入门", 1, new BigDecimal(100), new BigDecimal(100), "1234567890"));

    }
}