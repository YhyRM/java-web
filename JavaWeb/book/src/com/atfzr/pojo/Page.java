package com.atfzr.pojo;

import java.util.List;

/**
 * @author silence
 * @date 2022/9/18---9:21
 * @SuppressWarnings({"all"})
 */

/**
 *  Page是分页的模板对象
 * @param <T> 是具体的模块的javaBean类
 */
public class Page<T> {

    public static final Integer PAGE_SIZE = 4;
    //当前页码
    private Integer pageNo;
    //总页码
    private  Integer pageTotal;
    //当前页显示数量
    private  Integer pageSize =  PAGE_SIZE;
    //总记录数
    private  Integer PageTotalCount;
    //当前页数据
    private List<T> items;
    // 分页条的请求地址(为了区分client，manager等等)
    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }




    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageTotalCount() {
        return PageTotalCount;
    }

    public void setPageTotalCount(Integer PageTotalCount) {
        this.PageTotalCount = PageTotalCount;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {

//        // 数据边界的有效检查
//        if(pageNo < 1){
//            pageNo = 1;
//        }else if(pageNo > pageTotal){
//            pageNo= pageTotal;
//        }

        this.pageNo = pageNo;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pageTotal=" + pageTotal +
                ", pageSize=" + pageSize +
                ", PageTotalCount=" + PageTotalCount +
                ", items=" + items +
                ", url='" + url + '\'' +
                '}';
    }
}
