package com.atfzr.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author silence
 * @date 2022/9/22---21:46
 * @SuppressWarnings({"all"})   总订单 用订单编号 对应订单项
 */
public class Order {
    // 订单号(唯一)
    private  String orderId;
    private Date cretaeTime;
    private BigDecimal price;
    // 用数字表示物流的状态: 0未发货， 1已发货，2表示已签收
    private  Integer status = 0;
    private  Integer userId;

    public Order() {
    }

    public Order(String orderId, Date cretaeTime, BigDecimal price, Integer status, Integer userId) {
        this.orderId = orderId;
        this.cretaeTime = cretaeTime;
        this.price = price;
        this.status = status;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "order{" +
                "orderId='" + orderId + '\'' +
                ", cretaeTime=" + cretaeTime +
                ", price=" + price +
                ", status=" + status +
                ", userId=" + userId +
                '}';
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCretaeTime() {
        return cretaeTime;
    }

    public void setCretaeTime(Date cretaeTime) {
        this.cretaeTime = cretaeTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
