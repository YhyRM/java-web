package com.atfzr.service;

import com.atfzr.pojo.Cart;

/**
 * @author silence
 * @date 2022/9/23---8:29
 * @SuppressWarnings({"all"})
 */
public interface OrderService {
    public String createOrder(Cart cart , Integer userId);
}
