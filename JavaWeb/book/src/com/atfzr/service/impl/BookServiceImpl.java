package com.atfzr.service.impl;

import com.atfzr.dao.BookDao;
import com.atfzr.dao.impl.BookDaoImpl;
import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;
import com.atfzr.service.BookService;

import java.util.List;

/**
 * @author silence
 * @date 2022/9/17---9:37
 * @SuppressWarnings({"all"})  业务层方法重写Dao层的具体sql方法，主要是传参
 */
public class BookServiceImpl implements BookService {

    private BookDao bookDao = new BookDaoImpl();

    @Override
    public void addBook(Book book) {
        bookDao.addBook(book);
    }

    @Override
    public void deleteBookById(Integer id) {
        bookDao.deleteBookById(id);
    }

    @Override
    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    @Override
    public Book queryBookById(Integer id) {
        return bookDao.queryBookById(id);
    }

    @Override
    public List<Book> queryBooks() {
        return bookDao.queryBooks();
    }

    @Override
    public Page<Book> page(int pageNo, int pageSize) {
        Page<Book> page = new Page<>();
        //设置当前页码
        page.setPageNo(pageNo);
        //设置每页显示的数量
        page.setPageSize(pageSize);
        //总记录数
        Integer PageTotalCount = bookDao.queryForPageTotalCount();
        page.setPageTotalCount(PageTotalCount);
        //求总页码
        int pageTotal = PageTotalCount / pageSize;
        if (pageTotal % pageSize > 0) {
            pageTotal++;
        }
        page.setPageTotal(pageTotal);

        // 求当前页数据的开头索引
        int begin = (page.getPageNo() - 1) * pageSize;
//        int begin = (Math.max((pageNo -1 ),0)) * pageSize;
        List<Book> items = bookDao.queryForPageItems(begin, pageSize);
        page.setItems(items);

        return page;
    }

    @Override
    public Page<Book> pageByPrice(int pageNo, int pageSize, int min, int max) {
        Page<Book> page = new Page<>();
        //设置当前页码
        page.setPageNo(pageNo);
        //设置每页显示的数量
        page.setPageSize(pageSize);
        //总记录数
        Integer PageTotalCount = bookDao.queryForPageTotalCountByPrice(min,max);
        page.setPageTotalCount(PageTotalCount);
        //求总页码
        int pageTotal = PageTotalCount / pageSize;
        if (pageTotal % pageSize > 0) {
            pageTotal++;
        }
        page.setPageTotal(pageTotal);

        // 求当前页数据的开头索引
        int begin = (page.getPageNo() - 1) * pageSize;
//        int begin = (Math.max((pageNo -1 ),0)) * pageSize;
        List<Book> items = bookDao.queryForPageItemsByPrice(begin, pageSize,min,max);
        page.setItems(items);

        return page;
    }
}
