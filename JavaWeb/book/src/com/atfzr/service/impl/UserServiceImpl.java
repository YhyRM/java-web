package com.atfzr.service.impl;

import com.atfzr.dao.UserDao;
import com.atfzr.dao.impl.UserDaoImpl;
import com.atfzr.pojo.User;
import com.atfzr.service.UserService;

/**
 * @author silence
 * @date 2022/9/12---21:24
 * @SuppressWarnings({"all"})   完成对用户的登录和注册服务
 */
@SuppressWarnings({"all"})
public class UserServiceImpl implements UserService {

    //因为要查询数据库用户信息， 所以用UserDaoImpl类
    UserDao userDao = new UserDaoImpl();

    @Override
    public void registUser(User user) {
        userDao.saveUser(user);
    }

    //查询数据库中有无该用户
    @Override
    public User login(User user) {
      return   userDao.queryUserByUsernameAndPassword(user.getUsername(), user.getPassword());
    }

    @Override
    public boolean existsUsername(String username) {
        if(userDao.queryUserByUsername(username) == null){
            //为空就是不存在， 及可注册和添加
            return false;
        }

        return true;
    }
}
