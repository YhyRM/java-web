package com.atfzr.service.impl;

import com.atfzr.dao.BookDao;
import com.atfzr.dao.OrderDao;
import com.atfzr.dao.OrderItemDao;
import com.atfzr.dao.impl.BookDaoImpl;
import com.atfzr.dao.impl.OrderDaoImpl;
import com.atfzr.dao.impl.OrderItemDaoImpl;
import com.atfzr.pojo.*;
import com.atfzr.service.OrderService;

import java.util.Date;
import java.util.Map;

/**
 * @author silence
 * @date 2022/9/23---8:33
 * @SuppressWarnings({"all"}) 一个订单服务引用多个DAO
 */
public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao = new OrderDaoImpl();
    private OrderItemDao orderItemDao = new OrderItemDaoImpl();
    private BookDao bookDao = new BookDaoImpl();

    @Override
    public String createOrder(Cart cart, Integer userId) {
        System.out.println("OrderServiceImpl程序在[" + Thread.currentThread().getName() + "]中");

        //订单号===唯一性
        String orderId = System.currentTimeMillis() + "" + userId;
        // 创建一个订单对象
        Order order = new Order(orderId, new Date(), cart.getTotalPrice(), 0, userId);
        //保存订单到数据库
        orderDao.saveOrder(order);

        int i = 12 / 0; //故意出错， 看一下事务的作用

        // 遍历购物车中每一个商品项转换成为订单项保存到数据库
        for (Map.Entry<Integer, CartItem> entry : cart.getItems().entrySet()) {
            CartItem cartItem = entry.getValue();

            OrderItem orderItem = new OrderItem(null, cartItem.getName(), cartItem.getCount(), cartItem.getPrice(), cartItem.getTotalPrice(), orderId);
            //保存订单项到数据库
            orderItemDao.saveOrderItem(orderItem);

            // 更新【结算后】图书的库存和销量(生成订单的bug)
            Book book = bookDao.queryBookById(cartItem.getId());
            book.setStock(book.getStock() - cartItem.getCount());
            book.setSales(book.getSales() + cartItem.getCount());
            bookDao.updateBook(book);
        }

        //清空购物车
        cart.clear();
        return orderId;
    }

}
