package com.atfzr.service;

import com.atfzr.pojo.User;

/**
 * @author silence
 * @date 2022/9/12---21:14
 * @SuppressWarnings({"all"}) 规范注册业务， 一个业务一个方法
 */
public interface UserService {
    /**
     *  注册用户
     * @param user
     */
    public void registUser(User user);

    /**
     *  登录： 查询数据库中有无该用户
     * @param user
     * @return 返回null 登录失败  ， 反之成功
     */
    public User login(User user);

    /**
     *  判断用户名是否存在(是否已被占用)
     * @param username
     * @return 返回true 表示用户已经存在， false 表示用户名可以用
     */
    public boolean existsUsername(String username);
}
