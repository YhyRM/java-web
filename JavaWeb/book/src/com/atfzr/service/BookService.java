package com.atfzr.service;

import com.atfzr.pojo.Book;
import com.atfzr.pojo.Page;

import java.util.List;

/**
 * @author silence
 * @date 2022/9/17---9:33
 * @SuppressWarnings({"all"})
 */
public interface BookService {
    public void addBook(Book book);

    public void deleteBookById(Integer id);

    public void updateBook(Book book);

    public Book queryBookById(Integer id);

    public List<Book> queryBooks();

    Page<Book> page(int pageNo, int pageSize);

    Page<Book> pageByPrice(int pageNo, int pageSize, int min, int max);
}
