package com.atfzr.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author silence
 * @date 2022/9/12---10:31
 * @SuppressWarnings({"all"}) 完成对数据库的 连接和关闭
 */
@SuppressWarnings({"all"})
public class JdbcUtils {

    private static DruidDataSource dataSource;
    private  static ThreadLocal<Connection> conns = new ThreadLocal<>();

    static { //静态代码块，在类创建时调用
        try {
            Properties properties = new Properties();
            // 读取 jdbc.properties配置文件
            InputStream inputStream = JdbcUtils.class.getClassLoader().getResourceAsStream("jdbc.properties");
            //从流中加载数据
            properties.load(inputStream);

            //创建数据库连接池
            dataSource = (DruidDataSource) DruidDataSourceFactory.createDataSource(properties);
//            System.out.println(dataSource.getConnection()); //测试连接

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //一运行主方法 就会加载类，然后就会运行静态代码块
    public static void main(String[] args) {

    }

    /**
     * 获取数据库连接池中的连接
     *
     * @return 如果返回null， 说明获取连接失败
     * 返回值就是获取连接成功
     */
    public static Connection getConnection() {
        Connection conn = conns.get();

        if(conn == null){
            try {
                // 确定连接为同一个连接
                conn = dataSource.getConnection();

                conns.set(conn); // 保存到Thread对象中，供后面的jdbc 操作使用

                conn.setAutoCommit(false); // 设置为手动提交事务
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return conn;
    }

    /**
     *  提交事务，并关闭释放连接
     */
     public static void commitAndClose(){
         Connection connection = conns.get();
         if(connection != null){ // 如果不等于null， 说明之前使用过连接，操作过数据库

             try {
                 connection.commit(); //提交事务
                 connection.close(); //关闭事务
             } catch (SQLException e) {
                 e.printStackTrace();
             }
             // 一定要执行remove操作， 否则就会出错。 (因为Tomcat服务器底层使用了【线程池技术】)
             conns.remove();
         }
     }

    /**
     *  回滚事务， 关闭释放资源
     */
     public static void rollbackAndClose(){
         Connection connection = conns.get();
         if(connection != null){ // 如果不等于null， 说明之前使用过连接，操作过数据库

             try {
                connection.rollback(); //回滚事务,直接回滚到事务提交前的状态。
                 connection.close(); //关闭事务
             } catch (SQLException e) {
                 e.printStackTrace();
             }
             // 一定要执行remove操作， 否则就会出错。 (因为Tomcat服务器底层使用了【线程池技术】)
             conns.remove();
         }
     }


    /**
     * 关闭连接， 放回数据库连接池
     *
     * @param conn
     */
//    public static void close(Connection conn) {
//        //祖传判空， 不判有可能空指针异常
//        if(conn != null){
//            try {
//                conn.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
}
