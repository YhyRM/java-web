package com.atfzr.utils;

import com.atfzr.pojo.User;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author silence
 * @date 2022/9/16---17:05
 * @SuppressWarnings({"all"})  将【获取并保存请求参数】的操作封装成一个方法工具类
 */
public class WebUtils {

    /**
     * 将Map中的值注入到对应的JavaBean中, 返回 【保存了获取请求参数的对象】
     * @param value
     * @param bean
     * HttpServletRequest
     * Dao层
     * Service层
     * Web层 耦合度高
     * 用Map的原因是 高内聚低耦合
     */
    public static <T>  T  copyParamToBean(Map<String, String[]> value, T bean){
        try {
            User user = new User();
            System.out.println("注入之前: " + bean);
            /** 把所有请求的参数都注入到user对象中*/
            BeanUtils.populate(bean, value);
            System.out.println("注入之后: " + bean);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return  bean;
    }

    /**
     *  将字符串转换成为int类型的数据的操作(搞成工具类)
     * @param strInt
     * @param defaultValue
     * @return
     */
    public static int parseInt(String strInt, int defaultValue){
        try {
            return  Integer.parseInt(strInt);
        }catch (Exception e){
//            e.printStackTrace();
        }
        return  defaultValue;
    }
}
