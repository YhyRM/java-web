package com.atfzr.pojo;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author silence
 * @date 2022/9/9---14:17
 * @SuppressWarnings({"all"})
 */
@SuppressWarnings({"all"})
public class Dom4jTest {
    @Test
    public void test1() throws Exception{
        // 创建一个saxReader输入流， 去读取 xml 配置文件， 生成Document对象
        SAXReader saxReader = new SAXReader();

        Document document = saxReader.read("src/books.xml");

        System.out.println(document);
    }

    @Test
    public void test2() throws  Exception{
        //1. 【读取books.xml 文件】
        SAXReader reader = new SAXReader();
        // 在Junit测试中，相对路径是从模块名开始算的
        Document document = reader.read("src/books.xml");
        //2. 通过Document对象获取根元素
        Element rootElement = document.getRootElement();
//        System.out.println(rootElement);
        //3. 通过根元素获取book标签元素
        // 【element()和elements() 都是通过[子标签名]】【查找子元素】
        List<Element> books = rootElement.elements("book");
        //4. 遍历， 处理每个book标签转换为Book类
        for (Element book : books){
            // asXML() 把标签对象，转换为标签字符串
            Element nameElement = book.element("name");
            // getText(): 可以获取标签中的文本内容
            String nameText = nameElement.getText();
            // 直接 获取指定标签名的文本内容
            String priceText = book.elementText("price");
            String authorText = book.elementText("author");

            String snValue = book.attributeValue("sn");
            System.out.println(new Book(snValue,nameText, BigDecimal.valueOf(Double.parseDouble(priceText)),authorText));
        }
    }
}
