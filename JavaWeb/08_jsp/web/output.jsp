<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/2/23
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
//    out.write() 输出字符串没有问题
//    out.print() 输出任意数据都没有问题（都转换成为字符串后调用的 write 输出）

    //out 和 response 都有输出的方法
    //但out在页面执行完后会flush刷新， 直接到 response缓存区的底下
    out.write("out");
    response.getWriter().write("response");
//        out.write(12);
//        out.print(12);
%>
</body>
</html>
