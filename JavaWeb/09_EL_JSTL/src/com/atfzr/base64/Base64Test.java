package com.atfzr.base64;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;

/**
 * @author silence
 * @date 2022/9/15---16:31
 * @SuppressWarnings({"all"})  这种编码问题火狐已经解决了(所以了解就行)
 */
@SuppressWarnings({"all"})
public class Base64Test {
    public static void main(String[] args) throws Exception{
        String content = "这是需要Base64编码的内容";
        // 创建一个Base64编辑器
        BASE64Encoder base64Encoder = new BASE64Encoder();
        // 执行Base64编码操作
        String encodedString = base64Encoder.encode(content.getBytes("UTF-8"));
        // 解码操作
//        byte[] bytes = BASE64Decoder.decodeBuffer(encodedString);

//        String str = new String(bytes,"UTF-8");

        System.out.println(encodedString);
    }


}
