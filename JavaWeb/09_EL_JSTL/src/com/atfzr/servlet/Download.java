package com.atfzr.servlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @author silence
 * @date 2022/9/15---15:32
 * @SuppressWarnings({"all"})  记不住没关系，要用的时候有印象就行了
 */
@SuppressWarnings({"all"})
public class Download extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 获取要下载的文件名
       String downloadFileName = "2.jpg";

       //2. 读取要下载的【文件内容】(通过ServletContext对象可以读取)
        ServletContext servletContext = getServletContext();
        //获取要下载的类型
        String mimeType = servletContext.getMimeType("/file/" + downloadFileName);
        System.out.println("下载的文件类型:" + mimeType);

        /***===================对浏览器的操作===================*/
        // 4. 在回传前，通过响应头告诉客户端返回的【数据库类型】
        resp.setContentType(mimeType);
        //content Disposition响应头，表示收到的数据怎么处理
        // attachment 表示附件， 表示下载使用
        // filename 表示指定下载的文件名[由程序员自定]
//        resp.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);
        resp.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode("中国.jpg", "UTF-8"));
        /***===================对浏览器的操作===================*/

        /**===================真正的IO流区间===================*/
        //5. 还要告诉客户端收到的数据是用于下载使用(还是使用响应头)
        // 第一个斜杠是到web目录的路径
        InputStream resourceAsStream = servletContext.getResourceAsStream("/file/" + downloadFileName);
        // 获取响应的输出流
       OutputStream outputStream = resp.getOutputStream();

        //3.     把下载的文件内容【回传】给客户端
        // 读取输入流读取到的全部数据，复制给输出流， 输出给用户（浏览器）
        IOUtils.copy(resourceAsStream,outputStream);
        /**===================真正的IO流区间===================*/
    }
}
