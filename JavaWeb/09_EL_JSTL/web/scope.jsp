<%--
  Created by IntelliJ IDEA.
  User: feng'zhi'ren
  Date: 2022/9/15
  Time: 8:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--EL 域对象后面加个Scope就是 获取四个特定域中的属性 的方法--%>
<%
 pageContext.setAttribute("key1","pageContext1");
    pageContext.setAttribute("key2", "pageContext2");
    request.setAttribute("key2", "request");
    session.setAttribute("key2", "session");
    application.setAttribute("key2", "application");
%>

${ applicationScope.key2}

</body>
</html>
