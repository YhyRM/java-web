package com.atfzr.json;

import com.atfzr.pojo.Person;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

/**
 * @author silence
 * @date 2022/9/26---12:38
 * @SuppressWarnings({"all"})
 */
public class PersonMapType extends TypeToken<HashMap<Integer, Person>> {
}
