package com.atfzr.json;

import com.atfzr.pojo.Person;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author silence
 * @date 2022/9/25---18:48
 * @SuppressWarnings({"all"}) 演示json 和 java互转
 */
@SuppressWarnings({"all"})
public class JsonTest {

    //javaBean 和 json 的互转
    @Test
    public void test1() {
        Person person = new Person(1, "智哥好帅!");
        // 创建Gson对象实例
        Gson gson = new Gson();
        // toJson方法可以把java对象转换成为json字符串
        String personJsonString = gson.toJson(person);
        System.out.println(personJsonString);
        // fromJson方法可以把json字符串转回java对象
        // 第一个参数是json字符串
        // 第二个参数是转换回去的java对象类型
        Person person1 = gson.fromJson(personJsonString, Person.class);
        System.out.println(person1);
    }

    // List 和 json 的互转
    @Test
    public void test2() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1, "无名"));
        personList.add(new Person(2, "康师傅"));
        Gson gson = new Gson();
        // 把 List 转换为 json 字符串
        String personListJsonString = gson.toJson(personList);
        System.out.println(personListJsonString);
        List<Person> list = gson.fromJson(personListJsonString, new PersonListType().getType());
        System.out.println(list);
        Person person = list.get(0);
        System.out.println(person);
    }

    // map 和 json的互转
    @Test
    public void test3(){
        Map<Integer,Person>  personMap = new HashMap<>();

        personMap.put(1,new Person(1,"智哥好帅"));
        personMap.put(2,new Person(2,"康师傅也好帅"));

        Gson gson = new Gson();
        // 把map 集合转换成为 json字符串
        String personMapJsonString = gson.toJson(personMap);

        System.out.println(personMapJsonString);

//      Map<Integer,Person> personMap2  = gson.fromJson(personMapJsonString, new PersonMapType().getType());
        //还可以用 匿名内部类对象的getType方法来 获取类型
        Map<Integer,Person> personMap2  = gson.fromJson(personMapJsonString, new TypeToken<HashMap<Integer,Person>>(){}.getType());

        System.out.println(personMap);
        Person p = personMap2.get(1);
        System.out.println(p);
    }
}
