package com.atfzr.json;

import com.atfzr.pojo.Person;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * @author silence
 * @date 2022/9/25---18:59
 * @SuppressWarnings({"all"})
 */
public class PersonListType extends TypeToken<ArrayList<Person>> {
}
